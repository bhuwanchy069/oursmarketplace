az group create --name myResourceGroup --location southindia
az aks create --resource-group myResourceGroup --name myAKSCluster --node-count 2 --enable-addons monitoring --generate-ssh-keys

az aks show --name myAKSCluster --resource-group myResourceGroup

az acr create --resource-group myResourceGroup --name acraks069 --sku Standard --location southindia

az aks get-credentials --resource-group myResourceGroup --name myAKSCluster --overwrite-existing

az aks update -n myAKSCluster -g myResourceGroup --attach-acr acraks069

az group delete --name myResourceGroup --yes --no-wait

ALTER USER 'devil'@'%' IDENTIFIED BY 't00rt00r';
CREATE USER 'anonymous'@'localhost' IDENTIFIED BY 'p@ssw0rd';

GRANT ALL PRIVILEGES ON marketplace . * TO 'doadmin'@'%';

FLUSH PRIVILEGES;


python manage.py flush --no-input
python manage.py migrate --no-input
python manage.py collectstatic --no-input --clear



GKS
gcloud auth login
./google-cloud-sdk/bin/gcloud init    --no-launch-browser
kubectl config get-contexts
./google-cloud-sdk/bin/gcloud container clusters get-credentials myk8s-cluster --zone us-central1-c --project myk8s-project-344906

docker image build -t gcr.io/<project>/imgname:tag
gcloud projects list
gcloud auth configure-docker
gcloud container images list