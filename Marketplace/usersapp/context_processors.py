from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from .models import Subscribers
from marketapp.models import Listingproducts, Subcategory, Category,Listingimages
from usersapp.models import Userprofile
from .forms import SubscriberForm
from django.contrib.auth.decorators import login_required
from django.db.models import Q
def Subscriber(request):
    # subsemail = Subscribers.objects.all() 
    form = SubscriberForm()
    if request.method == 'POST':
        if 'subscriberform' in request.POST:
            form = SubscriberForm(request.POST)
            if form.is_valid():
                form.save()
                # return redirect('mainpage')
            else:
                print(f"Subscriberform is not Valid ...")
        else:
            print(f"NOT Subscriberform is calling ...")
    else:
        pass
        # print(f"NOT REQUEST METHOD CALLING ...")
    return {'subscriber':form}
   
def TopRatedSlides(request):
    images = Listingimages.objects.all()
    return {'brandnew': images,}

def MainSearchbar(request):
    searchproduct_query = ''
    searchlocation_query = ''
    print(f"searchQuery: {request.GET.get('searchproduct_query')}")
    if request.GET.get('searchproduct_query') or request.GET.get('searchlocation_query'):
        searchproduct_query = request.GET.get('searchproduct_query')
        searchlocation_query = request.GET.get('searchlocation_query')
    products = Listingproducts.objects.distinct().filter(
        Q(title__icontains=searchproduct_query)
        ).prefetch_related('listingimages_set')
    
    return {'products': products}
    

def AccountsProfile(request): 
    if request.user.is_authenticated:
        print("USerIs Authenticated")
        profile_obj = Userprofile.objects.get(user=request.user)
        return {'myprofile':profile_obj}

    else:
        print("Not Authenticated User")
    return {}
