#!/bin/sh

if [ "$DATABASE" = "mysql" ]
then
    echo "Waiting for Mysql to start..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 6
    done

    echo "MYSQL started succesfully !!!"
fi
python manage.py flush --no-input
python manage.py collectstatic --no-input --clear
gunicorn Marketplace.wsgi:application --bind 0.0.0.0:8000
exec "$@"
